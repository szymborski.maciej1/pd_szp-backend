FROM maven:3.8.5-openjdk-17 as builder
COPY src src
COPY pom.xml .
RUN mvn -f pom.xml clean package -DskipTests

FROM amazoncorretto:17
ARG JAR_FILE=target/*.jar
COPY --from=builder ${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]