# ITEX APP

## Table of Contents

* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Setup](#setup)
* [Project Status](#project-status)
* [Contact](#contact)
* [License](#license)

## General Information

- Project is a part of Engineering Thesis
- Frontend part of project can be found [here](https://gitlab.com/szymon242820/pd_szp-frontend)
- Purpose of project is to successfully pass engineering thesis
- After engineering thesis defence project will be continued as PoC project

## Technologies Used

- Spring version 2.7.1
- MySQL version 8.0.29
- Keycloak version 19.0.1
- Flyway version 6.3.1
- Docker compose version 3.8

## Setup

### Docker

To build docker image simply at root directory use `docker compose up --build`

Optionally add `-d` to detach building process from terminal

## Project Status

Project is: _in progress_

## Contact

Created by [@Szymborski Maciej](https://gitlab.com/szymborski.maciej1) - feel free to contact me!

## License

This project is open source and available under
the [MIT License](https://gitlab.com/szymborski.maciej1/pd_szp-backend/-/blob/main/LICENSE)