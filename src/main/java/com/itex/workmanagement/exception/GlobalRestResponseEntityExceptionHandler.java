package com.itex.workmanagement.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;

@ControllerAdvice
public class GlobalRestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    @Qualifier("exceptionMapper")
    private final Map<Class<? extends BaseException>, ExceptionCode> exceptionCodeHashMap;

    @Autowired
    public GlobalRestResponseEntityExceptionHandler(Map<Class<? extends BaseException>, ExceptionCode> exceptionCodeHashMap) {
        this.exceptionCodeHashMap = exceptionCodeHashMap;
    }

    @ExceptionHandler(BaseException.class)
    protected ResponseEntity<Object> handleException(BaseException exception) {
        ExceptionResponse response = new ExceptionResponse();
        response.setMessage(exception.getMessage());
        if (exceptionCodeHashMap.containsKey(exception.getClass())) {
            return new ResponseEntity<>(response, exceptionCodeHashMap.get(exception.getClass()).getStatus());
        }
        return new ResponseEntity<>("something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
