package com.itex.workmanagement.exception;

import com.itex.workmanagement.developer.exception.DeveloperNotFoundException;
import com.itex.workmanagement.workday.exception.WorkdayNotFoundException;
import com.itex.workmanagement.workday.exception.WorkdayOwnershipMismatchException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ExceptionMap {

    @Bean(name = "exceptionMapper")
    public Map<Class<? extends BaseException>, ExceptionCode> getExceptionCode() {
        HashMap<Class<? extends BaseException>, ExceptionCode> map = new HashMap<>();

        map.put(DeveloperNotFoundException.class, ExceptionCode.DEVELOPER_NOT_FOUND);
        map.put(WorkdayOwnershipMismatchException.class, ExceptionCode.WORKDAY_OWNERSHIP_MISMATCH);
        map.put(WorkdayNotFoundException.class, ExceptionCode.WORKDAY_NOT_FOUND);

        return map;
    }
}
