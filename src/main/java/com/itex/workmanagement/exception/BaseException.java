package com.itex.workmanagement.exception;

import lombok.experimental.StandardException;

@StandardException
public class BaseException extends RuntimeException {
}
