package com.itex.workmanagement.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ExceptionCode {

    DEVELOPER_NOT_FOUND(HttpStatus.NOT_FOUND),
    WORKDAY_OWNERSHIP_MISMATCH(HttpStatus.FORBIDDEN),
    WORKDAY_NOT_FOUND(HttpStatus.NOT_FOUND);

    private final HttpStatus status;
}
