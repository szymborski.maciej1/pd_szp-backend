package com.itex.workmanagement.keycloak;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeycloakConfig {
    @Value("${keycloak.realm}")
    private String realm;
    @Value("${keycloak.clientId}")
    private String clientId;
    @Value("${keycloak.auth-server-url}")
    private String authUrl;
    @Value("${KEYCLOAK_REALM_USERNAME}")
    private String keycloakUsername;

    @Value("${KEYCLOAK_REALM_PASSWORD}")
    private String keycloakPassword;

    @Bean
    public Keycloak configureKeycloak() {
        return KeycloakBuilder.builder()
                .grantType("password")
                .serverUrl(authUrl)
                .realm(realm)
                .clientId(clientId)
                .username(keycloakUsername)
                .password(keycloakPassword)
                .build();
    }
}
