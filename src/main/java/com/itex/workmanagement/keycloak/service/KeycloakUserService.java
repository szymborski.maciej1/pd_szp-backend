package com.itex.workmanagement.keycloak.service;

import com.itex.workmanagement.keycloak.CredentialUtils;
import com.itex.workmanagement.keycloak.KeycloakConfig;
import com.itex.workmanagement.keycloak.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class KeycloakUserService {

    private final KeycloakConfig keycloakConfig;
    @Value("${keycloak.childRealm}")
    private String keycloakChildRealm;

    public void addUserWithRole(UserDto userDto, String role) {
        CredentialRepresentation credential = CredentialUtils
                .createPasswordCredentials(userDto.getPassword());
        UserRepresentation user = new UserRepresentation();
        user.setUsername(userDto.getUserName());
        user.setFirstName(userDto.getFirstname());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setCredentials(Collections.singletonList(credential));
        user.setEnabled(true);
        user.setEmailVerified(true);

        UsersResource instance = getUserInstance();
        instance.create(user);
        assignRoleToUser(userDto.getUserName(), role);

    }

    private void assignRoleToUser(String username, String role) {
        RoleRepresentation roleRepresentation = getRoleByName(role);
        UserResource userResource = findUserByUsername(username);
        userResource.roles().realmLevel().add(List.of(roleRepresentation));
    }

    public UserResource findUserByUsername(String username) {
        UsersResource instance = getUserInstance();
        List<UserRepresentation> userReps = instance.search(username).stream().filter(userRepresentation -> userRepresentation.getUsername().equals(username)).toList();
        if (!userReps.isEmpty()) {
            return instance.get(userReps.get(0).getId());
        }
        return null;
    }

    private RoleRepresentation getRoleByName(String roleName) {
        RolesResource instance = getRoleInstance();
        return instance.list().stream().filter(element -> element.getName().equals(roleName)).toList().get(0);
    }

    private UsersResource getUserInstance() {
        return keycloakConfig.configureKeycloak().realm(keycloakChildRealm).users();
    }

    private RolesResource getRoleInstance() {
        return keycloakConfig.configureKeycloak().realm(keycloakChildRealm).roles();
    }
}
