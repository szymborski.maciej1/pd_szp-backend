package com.itex.workmanagement.keycloak.listener;

import com.itex.workmanagement.keycloak.dto.UserDto;
import com.itex.workmanagement.keycloak.service.KeycloakUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class KeycloakUserImportListener {

    private final KeycloakUserService keycloakUserService;

    @EventListener(ApplicationReadyEvent.class)
    public void addKeycloakUsers() {
        UserDto ccm = new UserDto();
        ccm.setUserName("testccm");
        ccm.setFirstname("test");
        ccm.setLastName("ccm");
        ccm.setEmail("test.ccm@google.com");
        ccm.setPassword("password");
        UserDto developer = new UserDto();
        developer.setUserName("testdeveloper");
        developer.setFirstname("test");
        developer.setLastName("developer");
        developer.setEmail("test.developer@google.com");
        developer.setPassword("password");
        UserDto teamleader = new UserDto();
        teamleader.setUserName("testteamleader");
        teamleader.setFirstname("test");
        teamleader.setLastName("teamleader");
        teamleader.setEmail("test.teamleader@google.com");
        teamleader.setPassword("password");
        UserDto hr = new UserDto();
        hr.setUserName("testHR");
        hr.setFirstname("test");
        hr.setLastName("hr");
        hr.setEmail("test.hr@google.com");
        hr.setPassword("password");
        if (keycloakUserService.findUserByUsername(ccm.getUserName()) == null) {
            keycloakUserService.addUserWithRole(ccm, "CCM");
            log.info("ccm account successfully created");
        }
        if (keycloakUserService.findUserByUsername(developer.getUserName()) == null) {
            keycloakUserService.addUserWithRole(developer, "DEVELOPER");
            log.info("developer account successfully created");
        }
        if (keycloakUserService.findUserByUsername(teamleader.getUserName()) == null) {
            keycloakUserService.addUserWithRole(teamleader, "TEAM_LEADER");
            log.info("teamleader account successfully created");
        }
        if (keycloakUserService.findUserByUsername(hr.getUserName()) == null) {
            keycloakUserService.addUserWithRole(hr, "HR");
            log.info("hr account successfully created");
        }
    }
}
