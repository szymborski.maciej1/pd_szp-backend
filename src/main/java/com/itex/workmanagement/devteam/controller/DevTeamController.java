package com.itex.workmanagement.devteam.controller;

import com.itex.workmanagement.devteam.dto.DevTeamPageResponse;
import com.itex.workmanagement.devteam.service.DevTeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/${ApiVersion}/devteam")
public class DevTeamController {
    private final DevTeamService devTeamService;

    @GetMapping
    public ResponseEntity<DevTeamPageResponse> findPaged(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        return new ResponseEntity<>(devTeamService.findPaged(pageNumber, pageSize), HttpStatus.OK);
    }
}
