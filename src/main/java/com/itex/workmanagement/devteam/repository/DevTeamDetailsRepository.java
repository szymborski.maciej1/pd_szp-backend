package com.itex.workmanagement.devteam.repository;

import com.itex.workmanagement.devteam.model.DevTeam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DevTeamDetailsRepository extends JpaRepository<DevTeam,Long> {
}
