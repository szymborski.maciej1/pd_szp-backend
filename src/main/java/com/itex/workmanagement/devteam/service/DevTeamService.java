package com.itex.workmanagement.devteam.service;

import com.itex.workmanagement.devteam.dto.DevTeamPageResponse;
import com.itex.workmanagement.devteam.mapper.DevTeamMapper;
import com.itex.workmanagement.devteam.repository.DevTeamDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DevTeamService {
    private final DevTeamDetailsRepository devTeamDetailsRepository;
    private final DevTeamMapper devTeamMapper;

    public DevTeamPageResponse findPaged(Integer pageNumber, Integer pageSize) {
        return devTeamMapper.toPageResponse(devTeamDetailsRepository.findAll(PageRequest.of(pageNumber, pageSize)));
    }
}
