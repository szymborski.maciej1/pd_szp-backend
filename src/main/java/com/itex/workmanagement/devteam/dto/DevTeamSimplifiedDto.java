package com.itex.workmanagement.devteam.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DevTeamSimplifiedDto {
    private Long id;
    private String name;
}
