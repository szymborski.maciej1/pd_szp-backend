package com.itex.workmanagement.devteam.dto;

import com.itex.workmanagement.teamleader.dto.TeamLeaderSimplifiedDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DevTeamDto {
    private Long id;
    private String name;
    private TeamLeaderSimplifiedDto teamLeader;
}
