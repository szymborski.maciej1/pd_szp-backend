package com.itex.workmanagement.devteam.mapper;

import com.itex.workmanagement.devteam.dto.DevTeamDto;
import com.itex.workmanagement.devteam.dto.DevTeamPageResponse;
import com.itex.workmanagement.devteam.dto.DevTeamSimplifiedDto;
import com.itex.workmanagement.devteam.model.DevTeam;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DevTeamMapper {
    DevTeamDto toDto(DevTeam devTeam);

    DevTeam fromDto(DevTeamDto devTeamDto);

    List<DevTeamDto> toDtoList(List<DevTeam> devTeams);

    List<DevTeam> fromDtoList(List<DevTeamDto> devTeamDtos);

    DevTeamSimplifiedDto toSimplifiedDto(DevTeam devTeam);

    DevTeam fromSimplifiedDto(DevTeamSimplifiedDto devTeamSimplifiedDto);

    List<DevTeamSimplifiedDto> toSimplifiedDtoList(List<DevTeam> devTeams);

    List<DevTeam> fromSimplifiedDtoList(List<DevTeamSimplifiedDto> devTeamSimplifiedDtos);

    @Mapping(source = "content", target = "result")
    @Mapping(source = "size", target = "pageSize")
    @Mapping(source = "number", target = "pageNumber")
    DevTeamPageResponse toPageResponse(Page<DevTeam> devTeams);
}
