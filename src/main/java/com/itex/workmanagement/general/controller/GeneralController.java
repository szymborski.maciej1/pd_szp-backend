package com.itex.workmanagement.general.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/${ApiVersion}")
@Slf4j
public class GeneralController {

    @GetMapping("/userinfo")
    public ResponseEntity<Object> getUserInfo(@AuthenticationPrincipal Jwt jwt) {
        return new ResponseEntity<>(jwt.getClaims(), HttpStatus.OK);
    }
    //TODO logs color and separate to show actions clearly
    //TODO elastic stack
}
