package com.itex.workmanagement.ccm.mapper;

import com.itex.workmanagement.ccm.dto.CcmDto;
import com.itex.workmanagement.ccm.model.Ccm;
import com.itex.workmanagement.ccm.model.CcmPageResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CcmMapper {
    CcmDto toDto(Ccm ccm);

    Ccm fromDto(CcmDto ccmDto);

    List<CcmDto> toDtoList(List<Ccm> ccms);

    List<Ccm> fromDtoList(List<CcmDto> ccmDtos);

    @Mapping(source = "content", target = "result")
    @Mapping(source = "size", target = "pageSize")
    @Mapping(source = "number", target = "pageNumber")
    CcmPageResponse toPageResponse(Page<Ccm> ccms);
}
