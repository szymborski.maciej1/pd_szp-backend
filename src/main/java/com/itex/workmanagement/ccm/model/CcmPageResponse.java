package com.itex.workmanagement.ccm.model;

import com.itex.workmanagement.ccm.dto.CcmDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CcmPageResponse {
    private List<CcmDto> result;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPages;
    private Integer totalElements;
}
