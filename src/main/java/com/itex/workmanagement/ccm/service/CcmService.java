package com.itex.workmanagement.ccm.service;

import com.itex.workmanagement.ccm.mapper.CcmMapper;
import com.itex.workmanagement.ccm.model.CcmPageResponse;
import com.itex.workmanagement.ccm.repository.CcmRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CcmService {

    private final CcmRepository ccmRepository;
    private final CcmMapper ccmMapper;

    public CcmPageResponse findPaged(Integer pageNumber, Integer pageSize) {
        return ccmMapper.toPageResponse(ccmRepository.findAll(PageRequest.of(pageNumber, pageSize)));
    }
}
