package com.itex.workmanagement.ccm.controller;

import com.itex.workmanagement.ccm.model.CcmPageResponse;
import com.itex.workmanagement.ccm.service.CcmService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/${ApiVersion}/ccm")
public class CcmController {
    private final CcmService ccmService;

    @GetMapping
    public ResponseEntity<CcmPageResponse> findPaged(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        return new ResponseEntity<>(ccmService.findPaged(pageNumber, pageSize), HttpStatus.OK);
    }
}
