package com.itex.workmanagement.ccm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CcmDto {
    private Long id;
    private String name;
    private String surname;
    private String email;
}
