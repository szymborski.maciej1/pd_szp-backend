package com.itex.workmanagement.ccm.repository;

import com.itex.workmanagement.ccm.model.Ccm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CcmRepository extends JpaRepository<Ccm,Long> {
}
