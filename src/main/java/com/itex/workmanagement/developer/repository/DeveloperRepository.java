package com.itex.workmanagement.developer.repository;

import com.itex.workmanagement.developer.model.Developer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, Long> {
    Optional<Developer> findByEmail(String email);

    Optional<Developer> findById(Long id);
}
