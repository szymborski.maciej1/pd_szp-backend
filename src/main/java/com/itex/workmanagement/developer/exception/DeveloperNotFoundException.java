package com.itex.workmanagement.developer.exception;

import com.itex.workmanagement.exception.BaseException;
import lombok.experimental.StandardException;

@StandardException
public class DeveloperNotFoundException extends BaseException {
}
