package com.itex.workmanagement.developer.dto;

import com.itex.workmanagement.developer.model.ExperienceLevel;
import com.itex.workmanagement.devteam.dto.DevTeamSimplifiedDto;
import com.itex.workmanagement.skill.model.Skill;
import com.itex.workmanagement.specialization.model.Specialization;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class DeveloperDto {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private ExperienceLevel exp;
    private Specialization spec;
    private List<Skill> skills;
    private DevTeamSimplifiedDto devTeam;

}
