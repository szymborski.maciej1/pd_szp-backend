package com.itex.workmanagement.developer.dto;

import com.itex.workmanagement.developer.model.ExperienceLevel;
import com.itex.workmanagement.specialization.model.Specialization;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DeveloperSimplifiedDto {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private ExperienceLevel exp;
    private Specialization spec;
}
