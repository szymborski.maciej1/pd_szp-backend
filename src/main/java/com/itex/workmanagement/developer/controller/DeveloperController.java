package com.itex.workmanagement.developer.controller;

import com.itex.workmanagement.developer.dto.DeveloperPageResponse;
import com.itex.workmanagement.developer.service.DeveloperService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/${ApiVersion}/developer")
public class DeveloperController {
    private final DeveloperService developerService;

    @GetMapping
    public ResponseEntity<DeveloperPageResponse> findPaged(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        return new ResponseEntity<>(developerService.findPaged(pageNumber, pageSize), HttpStatus.OK);
    }
}
