package com.itex.workmanagement.developer.model;

import com.itex.workmanagement.devteam.model.DevTeam;
import com.itex.workmanagement.skill.model.Skill;
import com.itex.workmanagement.specialization.model.Specialization;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Developer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private String email;
    @Enumerated(EnumType.STRING)
    @Column(name = "experienceLevel")
    private ExperienceLevel exp;
    @ManyToOne
    private Specialization spec;
    @ManyToMany
    private List<Skill> skills;
    @ManyToOne
    private DevTeam devTeam;
}
