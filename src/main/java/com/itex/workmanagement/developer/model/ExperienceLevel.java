package com.itex.workmanagement.developer.model;

public enum ExperienceLevel {
    INTERN,
    JUNIOR,
    MID,
    SENIOR,
    PRO;
}
