package com.itex.workmanagement.developer.service;

import com.itex.workmanagement.developer.dto.DeveloperPageResponse;
import com.itex.workmanagement.developer.mapper.DeveloperMapper;
import com.itex.workmanagement.developer.repository.DeveloperRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeveloperService {
    private final DeveloperRepository developerRepository;
    private final DeveloperMapper developerMapper;

    public DeveloperPageResponse findPaged(Integer pageNumber, Integer pageSize) {
        return developerMapper.toPageResponse(developerRepository.findAll(PageRequest.of(pageNumber, pageSize)));
    }
}
