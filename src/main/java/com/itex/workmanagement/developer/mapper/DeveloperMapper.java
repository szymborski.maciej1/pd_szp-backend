package com.itex.workmanagement.developer.mapper;

import com.itex.workmanagement.developer.dto.DeveloperDto;
import com.itex.workmanagement.developer.dto.DeveloperPageResponse;
import com.itex.workmanagement.developer.dto.DeveloperSimplifiedDto;
import com.itex.workmanagement.developer.model.Developer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DeveloperMapper {
    DeveloperDto toDto(Developer developer);

    Developer fromDto(DeveloperDto developerDto);

    DeveloperSimplifiedDto toSimplifiedDto(Developer developer);

    Developer fromSimplifiedDto(DeveloperSimplifiedDto developerSimplifiedDto);

    List<DeveloperDto> toDtoList(List<Developer> developers);

    List<Developer> fromDtoList(List<DeveloperDto> developerDtos);

    List<DeveloperSimplifiedDto> toSimplifiedDtoList(List<Developer> developers);

    List<Developer> fromSimplifiedDtoList(List<DeveloperSimplifiedDto> developerSimplifiedDtos);

    @Mapping(source = "content",target = "result")
    @Mapping(source = "size",target = "pageSize")
    @Mapping(source = "number",target = "pageNumber")
    DeveloperPageResponse toPageResponse(Page<Developer> developers);
}
