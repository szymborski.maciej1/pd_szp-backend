package com.itex.workmanagement.teamleader.dto;

import com.itex.workmanagement.ccm.dto.CcmDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TeamLeaderDto {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private CcmDto ccm;
}
