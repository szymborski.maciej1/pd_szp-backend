package com.itex.workmanagement.teamleader.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TeamLeaderSimplifiedDto {
    private Long id;
    private String name;
    private String surname;
    private String email;
}
