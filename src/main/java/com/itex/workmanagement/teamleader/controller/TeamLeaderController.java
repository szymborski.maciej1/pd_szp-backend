package com.itex.workmanagement.teamleader.controller;

import com.itex.workmanagement.teamleader.dto.TeamLeaderPageResponse;
import com.itex.workmanagement.teamleader.service.TeamLeaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/${ApiVersion}/teamleader")
public class TeamLeaderController {
    private final TeamLeaderService teamLeaderService;

    @GetMapping
    public ResponseEntity<TeamLeaderPageResponse> findPaged(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        return new ResponseEntity<>(teamLeaderService.findPaged(pageNumber, pageSize), HttpStatus.OK);
    }
}
