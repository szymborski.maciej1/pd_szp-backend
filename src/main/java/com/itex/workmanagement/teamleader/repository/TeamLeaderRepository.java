package com.itex.workmanagement.teamleader.repository;

import com.itex.workmanagement.teamleader.model.TeamLeader;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamLeaderRepository extends JpaRepository<TeamLeader, Long> {
}
