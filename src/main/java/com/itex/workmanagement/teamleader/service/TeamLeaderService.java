package com.itex.workmanagement.teamleader.service;

import com.itex.workmanagement.teamleader.dto.TeamLeaderPageResponse;
import com.itex.workmanagement.teamleader.mapper.TeamLeaderMapper;
import com.itex.workmanagement.teamleader.repository.TeamLeaderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeamLeaderService {
    private final TeamLeaderRepository teamLeaderRepository;
    private final TeamLeaderMapper teamLeaderMapper;

    public TeamLeaderPageResponse findPaged(Integer pageNumber, Integer pageSize) {
        return teamLeaderMapper.toPageResponse(teamLeaderRepository.findAll(PageRequest.of(pageNumber, pageSize)));
    }
}
