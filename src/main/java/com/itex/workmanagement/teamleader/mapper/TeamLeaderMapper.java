package com.itex.workmanagement.teamleader.mapper;

import com.itex.workmanagement.teamleader.dto.TeamLeaderDto;
import com.itex.workmanagement.teamleader.dto.TeamLeaderPageResponse;
import com.itex.workmanagement.teamleader.dto.TeamLeaderSimplifiedDto;
import com.itex.workmanagement.teamleader.model.TeamLeader;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamLeaderMapper {
    TeamLeaderDto toDto(TeamLeader teamLeader);

    TeamLeader fromDto(TeamLeaderDto teamLeaderDto);

    List<TeamLeaderDto> toDtoList(List<TeamLeader> teamLeaders);

    List<TeamLeader> fromDtoList(List<TeamLeaderDto> teamLeaderDtos);

    TeamLeaderSimplifiedDto toSimplifiedDto(TeamLeader teamLeader);

    TeamLeader fromSimplifiedDto(TeamLeaderSimplifiedDto teamLeaderSimplifiedDto);

    List<TeamLeaderSimplifiedDto> toSimplifiedDtoList(List<TeamLeader> teamLeaders);

    List<TeamLeader> fromSimplifiedDtoList(List<TeamLeaderSimplifiedDto> teamLeaderSimplifiedDtos);

    @Mapping(source = "content", target = "result")
    @Mapping(source = "size", target = "pageSize")
    @Mapping(source = "number", target = "pageNumber")
    TeamLeaderPageResponse toPageResponse(Page<TeamLeader> teamLeaders);
}
