package com.itex.workmanagement.workday.exception;

import com.itex.workmanagement.exception.BaseException;
import lombok.experimental.StandardException;

@StandardException
public class WorkdayOwnershipMismatchException extends BaseException {
}
