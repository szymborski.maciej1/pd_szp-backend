package com.itex.workmanagement.workday.controller;

import com.itex.workmanagement.workday.dto.WorkdayDto;
import com.itex.workmanagement.workday.service.WorkdayService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/${ApiVersion}/workday")
public class WorkdayController {
    private final WorkdayService workdayService;

    @GetMapping("/me")
    public ResponseEntity<List<WorkdayDto>> findWorkdayByDateMonthAndDateYear(@AuthenticationPrincipal Jwt jwt, @RequestParam Integer month, @RequestParam Integer year) {
        return new ResponseEntity<>(workdayService.findWorkdayByDateMonthAndDateYear(jwt, month, year), HttpStatus.OK);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('TEAM_LEADER')")
    public ResponseEntity<List<WorkdayDto>> findWorkdayByDeveloperIdAndDateMonthAndDateYear(@RequestParam Long developerId, @RequestParam Integer month, @RequestParam Integer year) {
        return new ResponseEntity<>(workdayService.findWorkdayByDeveloperIdAndDateMonthAndDateYear(developerId, month, year), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<WorkdayDto> addWorkday(@AuthenticationPrincipal Jwt jwt, @RequestBody WorkdayDto workdayDto) {
        return new ResponseEntity<>(workdayService.addWorkday(jwt, workdayDto), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<WorkdayDto> updateWorkday(@AuthenticationPrincipal Jwt jwt, @RequestBody WorkdayDto workdayDto) {
        return new ResponseEntity<>(workdayService.updateWorkday(jwt, workdayDto), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<Jwt> deleteWorkdayById(@AuthenticationPrincipal Jwt jwt, @RequestParam Long id) {
        workdayService.deleteWorkdayById(jwt, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
