package com.itex.workmanagement.workday.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum WorkdayStatus {
    ACCEPTED("ACCEPTED"),
    DECLINED("DECLINED"),
    NEW("NEW");
    private final String value;

}
