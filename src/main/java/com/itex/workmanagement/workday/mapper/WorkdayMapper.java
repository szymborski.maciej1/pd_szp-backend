package com.itex.workmanagement.workday.mapper;

import com.itex.workmanagement.workday.dto.WorkdayDto;
import com.itex.workmanagement.workday.model.Workday;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WorkdayMapper {
    WorkdayDto toDto(Workday workday);

    Workday fromDto(WorkdayDto workdayDto);

    List<WorkdayDto> toDtoList(List<Workday> workdays);

    List<Workday> fromDtoList(List<WorkdayDto> workdayDtos);
}
