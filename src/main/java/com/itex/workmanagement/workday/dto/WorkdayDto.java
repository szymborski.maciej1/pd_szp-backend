package com.itex.workmanagement.workday.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.itex.workmanagement.workday.model.WorkdayStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class WorkdayDto {
    private Long id;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    private LocalDate date;
    private Double duration;
    private WorkdayStatus status;
}
