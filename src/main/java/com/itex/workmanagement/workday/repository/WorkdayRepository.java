package com.itex.workmanagement.workday.repository;

import com.itex.workmanagement.workday.model.Workday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WorkdayRepository extends JpaRepository<Workday, Long> {
    @Query(value = "SELECT * FROM workday WHERE developer_id = ?1 AND MONTH(date) = ?2 AND YEAR(date) = ?3", nativeQuery = true)
    List<Workday> findWorkdayByDeveloperIdAndDateMonthAndDateYear(Long developerId, Integer month, Integer year);
}
