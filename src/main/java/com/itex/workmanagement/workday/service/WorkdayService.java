package com.itex.workmanagement.workday.service;

import com.itex.workmanagement.developer.exception.DeveloperNotFoundException;
import com.itex.workmanagement.developer.model.Developer;
import com.itex.workmanagement.developer.repository.DeveloperRepository;
import com.itex.workmanagement.workday.dto.WorkdayDto;
import com.itex.workmanagement.workday.exception.WorkdayNotFoundException;
import com.itex.workmanagement.workday.exception.WorkdayOwnershipMismatchException;
import com.itex.workmanagement.workday.mapper.WorkdayMapper;
import com.itex.workmanagement.workday.model.Workday;
import com.itex.workmanagement.workday.repository.WorkdayRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WorkdayService {
    private static final String WORKDAY_ID_NOT_FOUND_MESSAGE = "Workday with id: %d cannot be found";
    private static final String WORKDAY_OWNERSHIP_MISMATCH_MESSAGE = "Currently logged in developer is not the owner of workday with id: %d";
    private static final String JWT_CLAIM_EMAIL = "email";
    private static final String DEVELOPER_ID_NOT_FOUND_MESSAGE = "Developer with id %d cannot be found";
    private static final String DEVELOPER_EMAIL_NOT_FOUND_MESSAGE = "Developer with email: %s cannot be found";
    private final WorkdayRepository workdayRepository;
    private final DeveloperRepository developerRepository;
    private final WorkdayMapper workdayMapper;

    public List<WorkdayDto> findWorkdayByDeveloperIdAndDateMonthAndDateYear(Long developerId, Integer month, Integer year) {
        Optional<Developer> loggedInDeveloper = developerRepository.findById(developerId);
        if (loggedInDeveloper.isPresent()) {
            return workdayMapper.toDtoList(workdayRepository.findWorkdayByDeveloperIdAndDateMonthAndDateYear(developerId, month, year));
        }
        throw new DeveloperNotFoundException(String.format(DEVELOPER_ID_NOT_FOUND_MESSAGE, developerId));
    }

    public List<WorkdayDto> findWorkdayByDateMonthAndDateYear(Jwt jwt, Integer month, Integer year) {
        Optional<Developer> loggedInDeveloper = developerRepository.findByEmail(jwt.getClaim(JWT_CLAIM_EMAIL));
        if (loggedInDeveloper.isPresent()) {
            return workdayMapper.toDtoList(workdayRepository.findWorkdayByDeveloperIdAndDateMonthAndDateYear(loggedInDeveloper.get().getId(), month, year));
        }
        throw new DeveloperNotFoundException(String.format(DEVELOPER_EMAIL_NOT_FOUND_MESSAGE, jwt.getClaim(JWT_CLAIM_EMAIL).toString()));
    }

    public WorkdayDto addWorkday(Jwt jwt, WorkdayDto workdayDto) {
        Optional<Developer> loggedInDeveloper = developerRepository.findByEmail(jwt.getClaim(JWT_CLAIM_EMAIL));
        Workday workday = workdayMapper.fromDto(workdayDto);
        if (loggedInDeveloper.isPresent()) {
            workday.setDeveloperId(loggedInDeveloper.get().getId());
            return workdayMapper.toDto(workdayRepository.save(workday));
        }
        throw new DeveloperNotFoundException(String.format(DEVELOPER_EMAIL_NOT_FOUND_MESSAGE, jwt.getClaim(JWT_CLAIM_EMAIL).toString()));
    }

    public WorkdayDto updateWorkday(Jwt jwt, WorkdayDto workdayDto) {
        Optional<Developer> loggedInDeveloper = developerRepository.findByEmail(jwt.getClaim(JWT_CLAIM_EMAIL));
        Workday workdayToUpdate = workdayMapper.fromDto(workdayDto);
        Optional<Workday> previousWorkday = workdayRepository.findById(workdayDto.getId());
        if (loggedInDeveloper.isPresent()) {
            if (previousWorkday.isPresent()) {
                if (previousWorkday.get().getDeveloperId().equals(loggedInDeveloper.get().getId())) {
                    workdayToUpdate.setDeveloperId(loggedInDeveloper.get().getId());
                    workdayRepository.save(workdayToUpdate);
                }
                throw new WorkdayOwnershipMismatchException(String.format(WORKDAY_OWNERSHIP_MISMATCH_MESSAGE, previousWorkday.get().getId()));
            }
            throw new WorkdayNotFoundException(String.format(WORKDAY_ID_NOT_FOUND_MESSAGE, workdayToUpdate.getId()));
        }
        throw new DeveloperNotFoundException(String.format(DEVELOPER_EMAIL_NOT_FOUND_MESSAGE, jwt.getClaim(JWT_CLAIM_EMAIL).toString()));
    }

    public void deleteWorkdayById(Jwt jwt, Long workdayId) {
        Optional<Workday> workday = workdayRepository.findById(workdayId);
        if (workday.isPresent()) {
            Optional<Developer> workdayOwner = developerRepository.findById(workday.get().getDeveloperId());
            if (workdayOwner.isPresent() && workdayOwner.get().getEmail().equals(jwt.getClaim(JWT_CLAIM_EMAIL))) {
                workdayRepository.deleteById(workdayId);
            } else {
                throw new WorkdayOwnershipMismatchException(String.format(WORKDAY_OWNERSHIP_MISMATCH_MESSAGE, workdayId));
            }
        } else {
            throw new WorkdayNotFoundException(String.format(WORKDAY_ID_NOT_FOUND_MESSAGE, workdayId));
        }
    }
}
