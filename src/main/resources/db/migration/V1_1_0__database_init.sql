CREATE TABLE `itex`.`skill`
(
    `id`          bigint PRIMARY KEY AUTO_INCREMENT,
    `name`        varchar(255),
    `description` varchar(255)
);

CREATE TABLE `itex`.`specialization`
(
    `id`          bigint PRIMARY KEY AUTO_INCREMENT,
    `name`        varchar(255),
    `description` varchar(255)
);

CREATE TABLE `itex`.`dev_team`
(
    `id`             bigint PRIMARY KEY AUTO_INCREMENT,
    `name`           varchar(255),
    `team_leader_id` bigint
);

CREATE TABLE `itex`.`developer`
(
    `id`               bigint PRIMARY KEY AUTO_INCREMENT,
    `email`            varchar(255),
    `name`             varchar(255),
    `surname`          varchar(255),
    `experience_level` ENUM ('INTERN', 'JUNIOR', 'MID', 'SENIOR', 'PRO'),
    `dev_team_id`      bigint,
    `spec_id`          bigint
);

CREATE TABLE `itex`.`developer_skills`
(
    `developer_id` bigint,
    `skills_id`    bigint
);

CREATE TABLE `itex`.`team_leader`
(
    `id`      bigint PRIMARY KEY AUTO_INCREMENT,
    `name`    varchar(255),
    `surname` varchar(255),
    `email`   varchar(255),
    `ccm_id`  bigint
);

CREATE TABLE `itex`.`ccm`
(
    `id`      bigint PRIMARY KEY AUTO_INCREMENT,
    `name`    varchar(255),
    `surname` varchar(255),
    `email`   varchar(255)
);

CREATE TABLE `itex`.`workday`
(
    `id`           bigint PRIMARY KEY AUTO_INCREMENT,
    `description`  varchar(2000),
    `date`         date,
    `duration`     double,
    `developer_id` bigint,
    `status`       varchar(50)
);

ALTER TABLE `itex`.`dev_team`
    ADD FOREIGN KEY (`team_leader_id`) REFERENCES `itex`.`team_leader` (`id`);

ALTER TABLE `itex`.`developer`
    ADD FOREIGN KEY (`dev_team_id`) REFERENCES `itex`.`dev_team` (`id`);

ALTER TABLE `itex`.`developer`
    ADD FOREIGN KEY (`spec_id`) REFERENCES `itex`.`specialization` (`id`);

ALTER TABLE `itex`.`developer_skills`
    ADD FOREIGN KEY (`developer_id`) REFERENCES `itex`.`developer` (`id`);

ALTER TABLE `itex`.`developer_skills`
    ADD FOREIGN KEY (`skills_id`) REFERENCES `itex`.`skill` (`id`);

ALTER TABLE `itex`.`team_leader`
    ADD FOREIGN KEY (`ccm_id`) REFERENCES `itex`.`ccm` (`id`);

ALTER TABLE `itex`.`workday`
    ADD FOREIGN KEY (`developer_id`) REFERENCES `itex`.`developer` (`id`);